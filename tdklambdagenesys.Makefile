#
#  Copyright (c) 2022    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : waynelewis
# email   : waynelewis@ess.ue
# Date    : 2020-07-08
# version : 0.0.0

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP:=tdklambdagenesysApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.proto)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard ../template/*.db)

SOURCES   += $(APPSRC)/tdklambdagenesys.c

DBDS   += $(APPSRC)/tdklambdagenesys_definitions.dbd

SCRIPTS += $(wildcard iocsh/*.iocsh)
SCRIPTS += $(wildcard ../iocsh/*.iocsh)

.PHONY: vlibs
vlibs:
